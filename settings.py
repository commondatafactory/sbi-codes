"""Manage settings
"""
import os
import configparser

from sqlalchemy.ext.declarative import declarative_base

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

config_auth = configparser.ConfigParser()
config_auth.read(os.path.join(BASE_DIR, "config.ini"))

TESTING = {"running": False}

Base = declarative_base()

ENVIRONMENT_OVERRIDES = [
    ('host', 'DATABASE_HOST'),
    ('port', 'DATABASE_PORT'),
    ('database', 'DATABASE_NAME'),
    ('username', 'DATABASE_USER'),
    ('password', 'DATABASE_PASSWORD'),
]
