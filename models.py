"""
SBI table defenition using sqlalchemy.
"""

import logging
import argparse

from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import JSONB
# from sqlalchemy.ext.declarative import declarative_base

from settings import Base
from settings import ENVIRONMENT_OVERRIDES

import db_helper

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


def main(args):
    """Table management."""
    engine = db_helper.make_engine(
        environment=ENVIRONMENT_OVERRIDES,
    )
    _session = db_helper.set_session(engine)

    if args.dropall:
        LOG.warning("DROPPING ALL DEFINED TABLES")
        # resets everything
        Base.metadata.drop_all(engine)

    LOG.warning("CREATING ALL DEFINED TABLES")
    # recreate tables
    Base.metadata.create_all(engine)



class SBICodeHierarchy(Base):
    """ SBI Code Hierarchy """

    __tablename__ = "sbicodes"
    __table_args__ = {"schema": "import"}
    code = Column(String(8), primary_key=True)
    title = Column(String)
    sbi_tree = Column(JSONB, nullable=False)
    qa_tree = Column(JSONB)


if __name__ == "__main__":
    desc = "Create/Drop defined model tables."
    inputparser = argparse.ArgumentParser(desc)

    inputparser.add_argument(
        "--dropall", action="store_true",
        default=False, help="Drop EVERYTHING WHATCH OUT!"
    )

    _args = inputparser.parse_args()
    main(_args)
