SBI codes import scripts.

Create a python virtual env and install the required packages:

    pip install -r requirements.dev.txt

Run the import which scrapes sbi codes from the cbs website.

    python models.py to create tables.
    python load_sbi_codes.py


Stored data in database table  can be found here "import.sbicodes".

The raw sbi code / description is stored and also
an SBI tree which is a tree like structure which combines similar sbi codes
combined with a QA tree a more convenient structure of the sbi codes.
