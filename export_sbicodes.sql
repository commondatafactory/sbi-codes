create table export.sbicodes_expanded as (
select 
	code, 
	title as omschrijving, 
	sbi_tree->'l1'->0 as l1,
	sbi_tree->'l1'->1 as l1d,
	sbi_tree->'l2'->0 as l2, 
	sbi_tree->'l2'->1 as l2d, 
	sbi_tree->'l3'->0 as l3,
	sbi_tree->'l3'->1 as l3d,
	sbi_tree->'l4'->0 as l4,
	sbi_tree->'l4'->1 as l4d,
	sbi_tree->'l5'->0 as l5,
	sbi_tree->'l5'->1 as l5d,
	qa_tree->'q1' as q1,
	qa_tree->'q2' as q2,
	qa_tree->'q3' as q3
	from import.sbicodes s);


